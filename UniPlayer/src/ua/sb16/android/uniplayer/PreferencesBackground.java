/*
 * Copyright (C) 2016 Shubhadeep Banerjee <shubhadeep.banerjee2011@yahoo.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ua.sb16.android.uniplayer;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;


public class PreferencesBackground extends PreferenceFragment
 implements Preference.OnPreferenceClickListener
{
	private Context mContext;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = getActivity();

		// Themes are 'pre-compiled' in themes-list: get all values
		// and append them to our newly created PreferenceScreen
		PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(mContext);
		final String[] entries = getResources().getStringArray(R.array.bg_entries);
		final String[] values = getResources().getStringArray(R.array.bg_values);
		for (int i = 0; i < entries.length; i++) {

			int key = Integer.parseInt(values[i]);

			final Preference pref = new Preference(mContext);
			pref.setPersistent(false);
			pref.setOnPreferenceClickListener(this);
			pref.setTitle(entries[i]);
			pref.setKey(""+key);
			screen.addPreference(pref);
		}
		setPreferenceScreen(screen);
	}

	@Override
	public boolean onPreferenceClick(Preference pref) {
		SharedPreferences.Editor editor = PlaybackService.getSettings(mContext).edit();
		editor.putString(PrefKeys.SELECTED_BG, pref.getKey());
		editor.apply();
		return true;
	}
}
