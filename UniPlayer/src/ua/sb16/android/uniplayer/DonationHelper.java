package ua.sb16.android.uniplayer;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by shubhadeep on 11-12-2016.
 */
public class DonationHelper {

    final public static String getDonationPackageName(Context context)
    {
        return (context.getPackageName() + "pro");
    }
    final public static boolean isPurchased(Context context)
    {
        PackageManager manager = context.getPackageManager();
        try {
            PackageInfo info=manager.getPackageInfo(getDonationPackageName(context),PackageManager.GET_META_DATA);
            String check = manager.getInstallerPackageName(getDonationPackageName(context));
            if (check.equalsIgnoreCase("com.android.vending"))
            {
                return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }

        return false;
    }
}
