/*
 * Copyright (C) 2015-2016 Adrian Ulrich <adrian@blinkenlights.ch>
 * Copyright (C) 2016 Shubhadeep Banerjee <shubhadeep.banerjee2011@yahoo.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 */

package ua.sb16.android.uniplayer;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.TypedValue;
import android.content.res.Resources.Theme;
import android.widget.Toast;

public class ThemeHelper {

	final public static int fetchAttrColor(int attr, Context mContext) {
		TypedValue typedValue = new TypedValue();
		TypedArray a = mContext.obtainStyledAttributes(typedValue.data, new int[] { attr });
		int color = a.getColor(0, 0);
		a.recycle();
		return color;
	}

	/**
	 * Calls context.setTheme() with given theme.
	 * Will automatically swap the theme with an alternative
	 * version if the user requested us to use it
	 */
	final public static int setTheme(Context context, int theme)
	{
		boolean isBgDark = isBgDark(context);
		//theme twik
		if(usesHoloTheme() == false) {
			int themeBase = getThemeBase(context);

			switch (theme) {
				case R.style.Playback:
					if(!isBgDark){
						if(themeBase == R.style.UniBaseMaterial)
							theme = R.style.UniBaseMaterial_Playback;
						else if(themeBase == R.style.UniBasePink)
							theme = R.style.UniBasePink_Playback;
						else if(themeBase == R.style.UniBaseCrimson)
							theme = R.style.UniBaseCrimson_Playback;
						else if(themeBase == R.style.UniBaseTarq)
							theme = R.style.UniBaseTarq_Playback;
						else if(themeBase == R.style.UniBaseBlue)
							theme = R.style.UniBaseBlue_Playback;
						else if(themeBase == R.style.UniBaseAsphalt)
							theme = R.style.UniBaseAsphalt_Playback;
						else if(themeBase == R.style.UniBaseCarrot)
							theme = R.style.UniBaseCarrot_Playback;
					}
					else {
						if(themeBase == R.style.UniBaseMaterial)
							theme = R.style.Dark_UniBaseMaterial_Playback;
						else if(themeBase == R.style.UniBasePink)
							theme = R.style.Dark_UniBasePink_Playback;
						else if(themeBase == R.style.UniBaseCrimson)
							theme = R.style.Dark_UniBaseCrimson_Playback;
						else if(themeBase == R.style.UniBaseTarq)
							theme = R.style.Dark_UniBaseTarq_Playback;
						else if(themeBase == R.style.UniBaseBlue)
							theme = R.style.Dark_UniBaseBlue_Playback;
						else if(themeBase == R.style.UniBaseAsphalt)
							theme = R.style.Dark_UniBaseAsphalt_Playback;
						else if(themeBase == R.style.UniBaseCarrot)
							theme = R.style.Dark_UniBaseCarrot_Playback;
					}
					break;
				case R.style.Library:
					if(!isBgDark){
						if(themeBase == R.style.UniBaseMaterial)
							theme = R.style.UniBaseMaterial_Library;
						else if(themeBase == R.style.UniBasePink)
							theme = R.style.UniBasePink_Library;
						else if(themeBase == R.style.UniBaseCrimson)
							theme = R.style.UniBaseCrimson_Library;
						else if(themeBase == R.style.UniBaseTarq)
							theme = R.style.UniBaseTarq_Library;
						else if(themeBase == R.style.UniBaseBlue)
							theme = R.style.UniBaseBlue_Library;
						else if(themeBase == R.style.UniBaseAsphalt)
							theme = R.style.UniBaseAsphalt_Library;
						else if(themeBase == R.style.UniBaseCarrot)
							theme = R.style.UniBaseCarrot_Library;
					}
					else {
						if(themeBase == R.style.UniBaseMaterial)
							theme = R.style.Dark_UniBaseMaterial_Library;
						else if(themeBase == R.style.UniBasePink)
							theme = R.style.Dark_UniBasePink_Library;
						else if(themeBase == R.style.UniBaseCrimson)
							theme = R.style.Dark_UniBaseCrimson_Library;
						else if(themeBase == R.style.UniBaseTarq)
							theme = R.style.Dark_UniBaseTarq_Library;
						else if(themeBase == R.style.UniBaseBlue)
							theme = R.style.Dark_UniBaseBlue_Library;
						else if(themeBase == R.style.UniBaseAsphalt)
							theme = R.style.Dark_UniBaseAsphalt_Library;
						else if(themeBase == R.style.UniBaseCarrot)
							theme = R.style.Dark_UniBaseCarrot_Library;
					}
					break;
				case R.style.BackActionBar:
					if(!isBgDark){
						if(themeBase == R.style.UniBaseMaterial)
							theme = R.style.UniBaseMaterial_BackActionBar;
						else if(themeBase == R.style.UniBasePink)
							theme = R.style.UniBasePink_BackActionBar;
						else if(themeBase == R.style.UniBaseCrimson)
							theme = R.style.UniBaseCrimson_BackActionBar;
						else if(themeBase == R.style.UniBaseTarq)
							theme = R.style.UniBaseTarq_BackActionBar;
						else if(themeBase == R.style.UniBaseBlue)
							theme = R.style.UniBaseBlue_BackActionBar;
						else if(themeBase == R.style.UniBaseAsphalt)
							theme = R.style.UniBaseAsphalt_BackActionBar;
						else if(themeBase == R.style.UniBaseCarrot)
							theme = R.style.UniBaseCarrot_BackActionBar;
					}
					else {
						if(themeBase == R.style.UniBaseMaterial)
							theme = R.style.Dark_UniBaseMaterial_BackActionBar;
						else if(themeBase == R.style.UniBasePink)
							theme = R.style.Dark_UniBasePink_BackActionBar;
						else if(themeBase == R.style.UniBaseCrimson)
							theme = R.style.Dark_UniBaseCrimson_BackActionBar;
						else if(themeBase == R.style.UniBaseTarq)
							theme = R.style.Dark_UniBaseTarq_BackActionBar;
						else if(themeBase == R.style.UniBaseBlue)
							theme = R.style.Dark_UniBaseBlue_BackActionBar;
						else if(themeBase == R.style.UniBaseAsphalt)
							theme = R.style.Dark_UniBaseAsphalt_BackActionBar;
						else if(themeBase == R.style.UniBaseCarrot)
							theme = R.style.Dark_UniBaseCarrot_BackActionBar;
					}
					break;
				case R.style.PopupDialog:
					if(!isBgDark){
						if(themeBase == R.style.UniBaseMaterial)
							theme = R.style.UniBaseMaterial_PopupDialog;
						else if(themeBase == R.style.UniBasePink)
							theme = R.style.UniBasePink_PopupDialog;
						else if(themeBase == R.style.UniBaseCrimson)
							theme = R.style.UniBaseCrimson_PopupDialog;
						else if(themeBase == R.style.UniBaseTarq)
							theme = R.style.UniBaseTarq_PopupDialog;
						else if(themeBase == R.style.UniBaseBlue)
							theme = R.style.UniBaseBlue_PopupDialog;
						else if(themeBase == R.style.UniBaseAsphalt)
							theme = R.style.UniBaseAsphalt_PopupDialog;
						else if(themeBase == R.style.UniBaseCarrot)
							theme = R.style.UniBaseCarrot_PopupDialog;
					}
					else {
						if(themeBase == R.style.UniBaseMaterial)
							theme = R.style.Dark_UniBaseMaterial_PopupDialog;
						else if(themeBase == R.style.UniBasePink)
							theme = R.style.Dark_UniBasePink_PopupDialog;
						else if(themeBase == R.style.UniBaseCrimson)
							theme = R.style.Dark_UniBaseCrimson_PopupDialog;
						else if(themeBase == R.style.UniBaseTarq)
							theme = R.style.Dark_UniBaseTarq_PopupDialog;
						else if(themeBase == R.style.UniBaseBlue)
							theme = R.style.Dark_UniBaseBlue_PopupDialog;
						else if(themeBase == R.style.UniBaseAsphalt)
							theme = R.style.Dark_UniBaseAsphalt_PopupDialog;
						else if(themeBase == R.style.UniBaseCarrot)
							theme = R.style.Dark_UniBaseCarrot_PopupDialog;
					}
					break;
				default:
					throw new IllegalArgumentException("setTheme() called with unknown theme!");
			}
		}
		else
		{
			switch (theme) {
				case R.style.Playback:
					if (isBgDark) {
						theme = R.style.Playback;
					} else {
						theme = R.style.PlaybackLight;
					}
					break;
				case R.style.Library:
					if (isBgDark) {
						theme = R.style.Library;
					} else {
						theme = R.style.LibraryLight;
					}
					break;
				case R.style.BackActionBar:
					if (isBgDark) {
						theme = R.style.BackActionBar;
					} else {
						theme = R.style.BackActionBarLight;
					}
					break;
				case R.style.PopupDialog:
					if (isBgDark) {
						theme = R.style.PopupDialog;
					} else {
						theme = R.style.PopupDialogLight;
					}
					break;
				default:
					throw new IllegalArgumentException("setTheme() called with unknown theme!");
			}
		}
		context.setTheme(theme);
		return theme;
	}

	/**
	 * Helper function to get the correct play button drawable for our
	 * notification: The notification does not depend on the theme but
	 * depends on the API level
	 */
	final public static int getPlayButtonResource(boolean playing)
	{
		int playButton = 0;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			// Android >= 5.0 uses the dark version of this drawable
			playButton = playing ? R.drawable.widget_pause : R.drawable.widget_play;
		} else {
			playButton = playing ? R.drawable.pause : R.drawable.play;
		}
		return playButton;
	}

	/**
	 * Returns TRUE if we should use the dark material theme,
	 * Returns FALSE otherwise - always returns FALSE on pre-5.x devices
	 */
	final private static boolean usesDarkTheme(Context context)
	{
		boolean useDark = false;
		if(usesHoloTheme() == false) {
			//theme twik
			if(isBgDark(context))
				useDark = true;
		}
		return useDark;
	}

	final public static int getThemeBase(Context context)
	{
		TypedArray ar = context.getResources().obtainTypedArray(R.array.theme_styles);
		int themeBase = ar.getResourceId(getSelectedTheme(context), R.style.UniBaseMaterial);

		return themeBase;
	}
	/**
	 * Returns the user-selected theme id from the shared peferences provider
	 *
	 * @param context the context to use
	 * @return integer of the selected theme
	 */
	final private static int getSelectedTheme(Context context) {
		SharedPreferences settings = PlaybackService.getSettings(context);
		return Integer.parseInt(settings.getString(PrefKeys.SELECTED_THEME, PrefDefaults.SELECTED_THEME));
	}

	/**
	 * Returns the user-selected bg from the shared peferences provider
	 *
	 * @param context the context to use
	 * @return true if dark, else false
	 */
	final private static boolean isBgDark(Context context) {
		SharedPreferences settings = PlaybackService.getSettings(context);
		int bg = Integer.parseInt(settings.getString(PrefKeys.SELECTED_BG, PrefDefaults.SELECTED_BG));
		boolean useDark = true;
		if(bg == 0)
			useDark = false;

		return useDark;
	}

	/**
	 * Returns TRUE if this device uses the HOLO (android 4) theme
	 */
	final public static boolean usesHoloTheme() {
		return (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP);
	}

	/**
	 * Hacky function to get the colors needed to draw the default cover
	 * These colors should actually be attributes, but getting them programatically
	 * is a big mess
	 */
	final public static int[] getDefaultCoverColors(Context context) {
		int[] colors_holo_yolo         = { 0xff000000, 0xff606060, 0xff404040, 0x88000000 };
		int[] colors_material_light    = { 0xffeeeeee, 0xffd6d7d7, 0xffd6d7d7, 0x55ffffff };
		int[] colors_material_dark     = { 0xff303030, 0xff606060, 0xff404040, 0x33ffffff };
		int[] colors_marshmallow_light = { 0xfffafafa, 0xffd6d7d7, 0xffd6d7d7, 0x55ffffff };
		int[] colors_marshmallow_dark  = colors_material_dark;
		if (usesHoloTheme()) // pre material device
			return colors_holo_yolo;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			return usesDarkTheme(context) ? colors_marshmallow_dark : colors_marshmallow_light;
		// else
		return usesDarkTheme(context) ? colors_material_dark : colors_material_light;
	}

}
