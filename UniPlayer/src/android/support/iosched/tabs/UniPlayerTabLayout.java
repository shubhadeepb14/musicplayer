/*
 * Copyright (C) 2015-2016 Adrian Ulrich <adrian@blinkenlights.ch>
 * Copyright (C) 2016 Shubhadeep Banerjee <shubhadeep.banerjee2011@yahoo.com>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 */

package android.support.iosched.tabs;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import ua.sb16.android.uniplayer.R;
import ua.sb16.android.uniplayer.ThemeHelper;

/**
 * Simple wrapper for SlidingTabLayout which takes
 * care of setting sane per-platform defaults
 */
public class UniPlayerTabLayout extends SlidingTabLayout {

	public UniPlayerTabLayout(Context context) {
		this(context, null);
	}

	public UniPlayerTabLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public UniPlayerTabLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setSelectedIndicatorColors(context.getResources().getColor(ua.sb16.android.uniplayer.R.color.tabs_active_indicator));
		setDistributeEvenly(true);
	}

	/**
	 * Overrides the default text color
	 */
	@Override
	protected TextView createDefaultTabView(Context context) {
		TextView view = super.createDefaultTabView(context);
		//theme twik
		if(!ThemeHelper.usesHoloTheme()) {
			int themeBase = ThemeHelper.getThemeBase(context);
			if(themeBase == R.style.UniBaseMaterial)
				view.setTextColor(getResources().getColorStateList(R.color.tab_text_selector_base));
			if(themeBase == R.style.UniBasePink)
				view.setTextColor(getResources().getColorStateList(R.color.tab_text_selector_pink));
			if(themeBase == R.style.UniBaseCrimson)
				view.setTextColor(getResources().getColorStateList(R.color.tab_text_selector_crimson));
			if(themeBase == R.style.UniBaseTarq)
				view.setTextColor(getResources().getColorStateList(R.color.tab_text_selector_tarq));
			if(themeBase == R.style.UniBaseBlue)
				view.setTextColor(getResources().getColorStateList(R.color.tab_text_selector_blue));
			if(themeBase == R.style.UniBaseAsphalt)
				view.setTextColor(getResources().getColorStateList(R.color.tab_text_selector_asphalt));
			if(themeBase == R.style.UniBaseCarrot)
				view.setTextColor(getResources().getColorStateList(R.color.tab_text_selector_carrot));
		}
		else
		{
			view.setTextColor(getResources().getColorStateList(R.color.tab_text_selector_holo));
		}
		view.setBackgroundResource(ua.sb16.android.uniplayer.R.drawable.unbound_ripple_light);
		view.setMaxLines(1);
		view.setEllipsize(TextUtils.TruncateAt.END);
		view.setTextSize(17);
		return view;
	}

}
