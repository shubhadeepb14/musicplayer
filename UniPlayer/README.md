UniPlayer
=====================

UniPlayer player is a [GPLv3](LICENSE) licensed music player for Android. This app is based on the open source Vanilla Music app.

Google Play store link : http://play.google.com/store/apps/details?id=ua.sb16.android.uniplayer

Donations
===========
You can donate to UniPlayer development by downloading the UniPlayer donation app from play store

Link : http://play.google.com/store/apps/details?id=ua.sb16.android.uniplayerpro